$(document).ready(function(){
	// init Isotope
	var $grid = $('.grid');
	
	// filter items on button click
	$('.filter-button-group').on('click', 'button', function() {
		var filterValue = $(this).attr('data-filter');
		$grid.isotope({ filter: filterValue });
	});
	
	// init Magnific-Popup
	$('.img-popup').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: true
		}, zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
	$('.img-popup-more').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: true
		}
	});
	
	$('.hkgg-gallery').magnificPopup({
		type: 'image',
		items: [
		  {
			src: 'img/hkgg/hkgg1.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg2.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg3.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg4.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg5.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg6.jpg'
		  },
		  {
			src: 'img/hkgg/hkgg7.jpg'
		  }
		],
		gallery: {
		  enabled: true
		}
	});
});